//
//  ViewController.swift
//  InfoKayakDemo
//
//  Created by bwc on 17/05/2018.
//  Copyright © 2018 bwc. All rights reserved.
//

import UIKit
import Alamofire


class ViewController: UIViewController {

    @IBOutlet weak var niveauLabel: UILabel!
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var meteoLabel: UILabel!
    
    let currentRiverSearch = "?dataset=hauteurs-deau-et-debits-des-cours-deau-observes-en-temps-reel-aux-stations-du-re&rows=1&start=0&refine.lbstationhydro=La+Loire+%C3%A0+Digoin"
    
    let forecastAPIKey = "abcdefghijklemnopqrstuvwxyz123456789" //Remplacer la clé par celle obtenue sur Darksky.net
    let coordinate: (lat: Double, long: Double) = (46.4706454,4.3356467)
    
    var forecastWeatherService: ForecastWeatherService!
    
    var forecastRiverService: ForecastRiverService!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
      
        loadRiverDatas()
        
        loadWeatherDatas()
        
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK : Actions
    
    @IBAction func refreshButton(_ sender: UIButton) {
        
        loadRiverDatas()
        
        loadWeatherDatas()
    }
    
    
    
    //MARK: Utilities
    
    private func loadRiverDatas() -> Void {
        forecastRiverService = ForecastRiverService()
        forecastRiverService.getCurrentRiver(currentSearch: currentRiverSearch) { (currentRiver) in
            if let currentRiver = currentRiver {
                DispatchQueue.main.async {
                    if let hauteur = currentRiver.hauteur {
                        self.niveauLabel.text = "\(hauteur) m"
                        print("niveau = " , hauteur)
                    } else {
                        self.niveauLabel.text = "0.00 m"
                        print("not found")
                    }
                    
                }
            }
            
        }
    }
    
    private func loadWeatherDatas() -> Void {
        forecastWeatherService = ForecastWeatherService(APIKey: forecastAPIKey)
        forecastWeatherService.getCurrentWeather(latitude: coordinate.lat, longitude: coordinate.long) { (currentWeather) in
            if let currentWeather = currentWeather {
                DispatchQueue.main.async {
                    if var temperature = currentWeather.temperature {
                        
                        temperature = fahrenheitToCelsius(tempInFahrenheit: temperature)
                        temperature = roundToPlaces(value: temperature, places: 2)
                        
                        self.temperatureLabel.text = "\(temperature)°"
                    } else {
                        self.temperatureLabel.text = "-"
                    }
                    if let summary = currentWeather.summary {
                        self.meteoLabel.text = "\(summary)"
                    } else {
                        self.meteoLabel.text = "-"
                    }
                }
            }
        }
        
    }
    
    // Pour mettre une image redimentionnées en fond d'écran
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        UIGraphicsBeginImageContext(view.frame.size)
        UIImage(named: "Background")?.draw(in: self.view.bounds)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        view.backgroundColor = UIColor.init(patternImage: image!)
       
    }

}

