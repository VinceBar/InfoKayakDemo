//
//  RoundToPlace.swift
//  InfoKayakDemo
//
//  Created by bwc on 19/05/2018.
//  Copyright © 2018 bwc. All rights reserved.
//

import Foundation

public func roundToPlaces(value:Double, places:Int) -> Double {
    let divisor = pow(10.0, Double(places))
    return round(value * divisor) / divisor
}

// fonction pour choisir le nombre de décimales d'un Double.
// Usage roundToPlaces(value:0.12345, places:2) -> 0.12
