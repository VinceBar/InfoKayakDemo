//
//  FahrenheitToCelsius.swift
//  InfoKayakDemo
//
//  Created by bwc on 19/05/2018.
//  Copyright © 2018 bwc. All rights reserved.
//

import Foundation

var tempInCelsius: Double = 0.00

public func fahrenheitToCelsius(tempInFahrenheit: Double) -> Double {
    tempInCelsius = (tempInFahrenheit - 32) * (5/9)
    return tempInCelsius
}

// Usage :
// Celsius = fahrenheitToCelsius(tempInFahrenheit: temperatureInFahrenheit)
