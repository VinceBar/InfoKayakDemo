//
//  CurrentRiver.swift
//  InfoKayakDemo
//
//  Created by bwc on 19/05/2018.
//  Copyright © 2018 bwc. All rights reserved.
//

import Foundation

class CurrentRiver {
    
    let lbstationhydro : String?
    let timestanp : String?
    let station_id : String?
    let debit : Double?
    let hauteur : Double?
    
    struct RiverKeys {
        static let lbstationhydro = "lbstationhydro"
        static let timestamp = "timestamp"
        static let station_id = "station_id"
        static let debit = "debit"
        static let hauteur = "hauteur"
    }
    
    init(riverDictionary: [String : Any]) {
        
        lbstationhydro = riverDictionary[RiverKeys.lbstationhydro] as? String
        timestanp = riverDictionary[RiverKeys.timestamp] as? String
        station_id = riverDictionary[RiverKeys.station_id] as? String
        debit = riverDictionary[RiverKeys.debit] as? Double
        hauteur = riverDictionary[RiverKeys.hauteur] as? Double
        
    }
    
}
