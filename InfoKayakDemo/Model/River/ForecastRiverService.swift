//
//  ForecastRiverService.swift
//  InfoKayakDemo
//
//  Created by bwc on 19/05/2018.
//  Copyright © 2018 bwc. All rights reserved.
//

import Foundation
import Alamofire

class ForecastRiverService {
    
    // Sample url : https://public.opendatasoft.com/api/records/1.0/search/?dataset=hauteurs-deau-et-debits-des-cours-deau-observes-en-temps-reel-aux-stations-du-re&refine.lbstationhydro=La+Loire+%C3%A0+Digoin
    
    let riverBaseURL = URL(string: "https://public.opendatasoft.com/api/records/1.0/search/")
    
    
    
    func getCurrentRiver(currentSearch: String, completion: @escaping (CurrentRiver?) -> Void) {
        if let riverURL = URL(string: "\(riverBaseURL!)\(currentSearch)") {
            
            Alamofire.request(riverURL).responseJSON(completionHandler: { (response) in
                
                if (response.result.isSuccess) {
                
                    if let jsonDictionary = response.result.value as? [String : Any] {
                    
                    
                        if let currentRiverDictionary = jsonDictionary["records"] as? [[String: Any]] {
                            
                           
                            
                            let currentRiver = CurrentRiver(riverDictionary: currentRiverDictionary[0]["fields"] as! [String : Any])
                            
                            completion(currentRiver)
                        } else {
                        completion(nil)
                    }
                }
                } else if (response.result.isFailure) {
                    //Manager your error
                    switch (response.error!._code){
                    case NSURLErrorTimedOut:
                        //Manager your time out error
                        print("Network Timeout !")
                        break
                    case NSURLErrorNotConnectedToInternet:
                        //Manager your not connected to internet error
                        print("You're not connected to Internet")
                        break
                    default:
                        //manager your default case
                        print(response.error!._code)
                    }
                }
            })
        }
    
    }
    
}
