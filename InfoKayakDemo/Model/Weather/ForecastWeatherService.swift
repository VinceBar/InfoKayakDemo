//
//  ForecastWeatherService.swift
//  InfoKayakDemo
//
//  Created by bwc on 19/05/2018.
//  Based on the Duc Tran Alamofire Tutorial.
//  Copyright © 2018 bwc. All rights reserved.
//

import Foundation
import Alamofire

class ForecastWeatherService
{
    // Sample url: https://api.darksky.net/forecast/33c371344898311931ea3058dcc4730f/37.8267,-122.4233
    let forecastAPIKey: String
    let forecastBaseURL: URL?
    
    init(APIKey: String)
    {
        self.forecastAPIKey = APIKey
        forecastBaseURL = URL(string: "https://api.darksky.net/forecast/\(APIKey)")
    }
    
    func getCurrentWeather(latitude: Double, longitude: Double, completion: @escaping (CurrentWeather?) -> Void)
    {
        if let forecastURL = URL(string: "\(forecastBaseURL!)/\(latitude),\(longitude)") {
            
            Alamofire.request(forecastURL).responseJSON(completionHandler: { (response) in
                
                if (response.result.isSuccess) {
                if let jsonDictionary = response.result.value as? [String : Any] {
                    
                    
                    if let currentWeatherDictionary = jsonDictionary["currently"] as? [String : Any] {
                        
                        let currentWeather = CurrentWeather(weatherDictionary: currentWeatherDictionary)
                        completion(currentWeather)
                    } else {
                        completion(nil)
                    }
                }
                } else if (response.result.isFailure) {
                    //Manager your error
                    switch (response.error!._code){
                    case NSURLErrorTimedOut:
                        //Manager your time out error
                        print("Network Timeout !")
                        break
                    case NSURLErrorNotConnectedToInternet:
                        //Manager your not connected to internet error
                        print("You're not connected to Internet")
                        break
                    default:
                        //manager your default case
                        print(response.error!._code)
                    }
                }
            })
        }
        
    }
}
